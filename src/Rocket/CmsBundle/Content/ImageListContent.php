<?php

namespace Rocket\CmsBundle\Content;

use Rocket\CmsBundle\Form\ImageListContentType;
use Symfony\Component\Form\Form;

/**
 * @author Fernando Carletti <fcarletti@rocket-internet.com.br>
 */
class ImageListContent extends AbstractContent
{

    /**
     * @return \Symfony\Component\Form\AbstractType
     */
    public function getForm()
    {
        $form = new ImageListContentType();

        return $form;
    }

    /**
     * @return string
     */
    public function getTemplateName()
    {
        return 'CmsBundle:Cms:imageList.html.twig';
    }

    /**
     * Return the form template path
     *
     * @return  string
     */
    public function formTemplate()
    {
        return 'CmsBundle::Cms/Form/imageList.html.twig';
    }

    /**
     * Process form contents
     *
     * @param \Symfony\Component\Form\Form $form
     * @param string                       $cmsFilesDir
     *
     * @return  array
     */
    public function processForm(Form $form, $cmsFilesDir = null)
    {
        $data = $form->getData();
        $className = $this->getClassName();
        $useS3 = $this->container->getParameter('cms.aws_s3_use');
        $saveAsset = $this->container->getParameter('cms.save_in_server');

        if (isset($data['remove_images'])) {
            foreach ($data['remove_images'] as $imageIndex) {
                if($useS3){
                    // Remove from S3
                    $bucket = $this->container->get('s3_manager');
                    $bucket->delete('cms/'.$data['images'][$imageIndex]);
                }

                unset($data['images'][$imageIndex]);
            }

            unset($data['remove_images']);
        }

        foreach ($data['image_files'] as $imageFile) {
            $extension = $imageFile->guessExtension();
            $fileName = sprintf('%s_%s.%s', $className, uniqid(), $extension);

            if($saveAsset){
                // Save file in server
                $imageFile->move($cmsFilesDir, $fileName);
            }            
            
            if($useS3){
                // Send to AWS S3
                $bucket = $this->container->get('s3_manager');
                $bucket->putFile('cms/' . $fileName, $cmsFilesDir . $fileName);
            }

            $data['images'][] = $fileName;
        }

        unset($data['image_files']);

        return $data;
    }
}
