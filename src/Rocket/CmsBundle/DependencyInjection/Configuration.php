<?php

namespace Rocket\CmsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('cms');
        $rootNode
            ->children()
                ->scalarNode('base_url')
                    ->info('Static files base url')
                    ->example('http://static.example.com/cms')
                    ->defaultNull()
                ->end()
                ->scalarNode('files_path')
                    ->info('Static files path')
                    ->example('/some/path/for/cms/files')
                    ->defaultNull()
                ->end()
                ->scalarNode('frontend_template')
                    ->info('Frontend template is used to display page content types')
                    ->example('AcmeDemoBundle::layout.html.twig')
                    ->defaultNull()
                ->end()
                ->scalarNode('backend_template')
                    ->info('Backend template is used to cms administration area')
                    ->example('AcmeBackendDemoBundle::layout.html.twig')
                    ->defaultNull()
                ->end()
                ->scalarNode('content_form_theme')
                    ->info('Theme for cms content properties')
                    ->example('AcmeDemoBundle::form.html.twig')
                    ->defaultNull()
                ->end()
                ->scalarNode('serialized_data_form_theme')
                    ->info('Theme for cms content serialized data')
                    ->example('AcmeDemoBundle::anotherForm.html.twig')
                    ->defaultNull()
                ->end()
                ->scalarNode('page_content_template')
                    ->info('Template file for PageContent')
                    ->example('CmsBundle:Cms:page.html.twig')
                    ->defaultValue('CmsBundle:Cms:page.html.twig')
                ->end()
                ->scalarNode('aws_s3_use')
                    ->info('Enable the AWS S3 Storage for CMS files')
                    ->example(true)
                    ->defaultValue(false)
                ->end()
                ->scalarNode('aws_s3_key')
                    ->info('AWS S3 Storage Access Key')
                    ->example('AHJKJTRYZ7KB7XNZIQ4Q')
                    ->defaultNull()
                ->end()
                ->scalarNode('aws_s3_secret')
                    ->info('AWS S3 Storage Access Secret')
                    ->example('23hJGxEWZpAFAV1Hhc+A5xe1++aBoBC98BeCTab1')
                    ->defaultNull()
                ->end()
                ->scalarNode('aws_s3_bucketname')
                    ->info('AWS S3 Storage Bucket name')
                    ->example('bucket-name')
                    ->defaultNull()
                ->end()
                ->scalarNode('save_in_server')
                    ->info('Save files to default assets server')
                    ->example(true)
                    ->defaultValue(true)
                ->end()
            ->end();

        return $treeBuilder;
    }
}
