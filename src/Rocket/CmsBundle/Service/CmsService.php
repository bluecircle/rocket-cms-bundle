<?php

namespace Rocket\CmsBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Rocket\CmsBundle\Entity\CmsContent;
use Rocket\CmsBundle\Content\BlankContent;
use Symfony\Component\DependencyInjection\Container;

/**
 * @author Fernando Carletti <fcarletti@rocket-internet.com.br>
 * @author Maykon Diógenes <maykon.diogenes@rocket-internet.com.br>
 */
class CmsService
{
    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    protected $container;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @param \Symfony\Component\DependencyInjection\Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->em = $this->container->get('doctrine.orm.default_entity_manager');
    }

    /**
     * @param string $name
     *
     * @param string $locale
     * @return \Rocket\CmsBundle\Content\AbstractContent
     */
    public function findByName($name, $locale = null)
    {
        $result = $this->findBy('name', $name, $locale);

        if (!$result) {
            $cmsContent = new CmsContent();
            $result = new BlankContent($cmsContent, $this->container);
        }

        return $result;
    }

    /**
     * @param string $uri
     * @param string $locale
     *
     * @return \Rocket\CmsBundle\Content\AbstractContent
     */
    public function findByUri($uri, $locale = null)
    {
        $result = $this->findBy('uri', $uri, $locale);

        if (!$result) {
            $cmsContent = new CmsContent();
            $result = new BlankContent($cmsContent, $this->container);
        }

        return $result;
    }

    /**
     * @param string $field
     * @param string $value
     * @param string $locale
     *
     * @throws \Exception
     * @return \Rocket\CmsBundle\Content\AbstractContent
     */
    protected function findBy($field, $value, $locale = null)
    {
        /** @var \Rocket\CmsBundle\Repository\CmsContentRepository $cmsContentRepository */
        $cmsContentRepository = $this->em->getRepository('CmsBundle:CmsContent');
        $cmsContent = $cmsContentRepository->findOneWithLocale($field, $value, $locale);

        if (!is_null($cmsContent)) {
            $result = $this->instantiate($cmsContent);
        } else {
            $result = null;
        }

        return $result;
    }

    /**
     * Dynamically create a class instance.
     *
     * @param \Rocket\CmsBundle\Entity\CmsContent $cmsContent
     * @param string                                          $forcedCmsContentClass Set another cms content class to render content in an alternative way
     *
     * @throws \Exception
     * @return \Rocket\CmsBundle\Content\AbstractContent
     */
    public function instantiate(CmsContent $cmsContent, $forcedCmsContentClass = null)
    {
        $class = $forcedCmsContentClass ?: $cmsContent->getCmsType()->getClass();

        if (!class_exists($class)) {
            throw new \Exception("Cms type {$class} not found!");
        }

        $reflection = new \ReflectionClass($class);
        $result = $reflection->newInstance($cmsContent, $this->container);

        return $result;
    }
}
